package com.passcan;

import com.fft.FourierTransform;
import org.jetbrains.annotations.NotNull;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.javacpp.opencv_core.CV_PI;
import static org.opencv.imgproc.Imgproc.RETR_LIST;
import static org.opencv.imgproc.Imgproc.drawContours;

public class StartLineRecognizer {
    private static String PASSPORTS_DIR = "src/main/resources/passcan/img/pass/";
    final double maxSideLength = 1000;

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
//        new StartLineRecognizer().rotateAndBinarize(PASSPORTS_DIR + "1.jpg", "dft.png");
//        for (int i = 1; i <= 15; i++) {
//            new StartLineRecognizer().rotateAndBinarize(1);
//        new StartLineRecognizer().rotateAndBinarize("C:\\Users\\Ashot\\Desktop\\code\\Kotlin\\passcan\\binarized.jpg", "mean.png");
//        }
        new StartLineRecognizer().rotateAndBinarize(14);
    }

    private void rotateAndBinarize(String in, String out) {
        List<Mat> list = new ArrayList<>();
        Mat originalImage = Imgcodecs.imread(in);
        Mat image = originalImage.clone();
        if (image.height() > maxSideLength || image.width() > maxSideLength) {
            System.out.println();
            Imgproc.resize(image, image, getCompressedSize(image.size()));
        }
//        Imgproc.warpAffine(image, image,
//                Imgproc.getRotationMatrix2D(
//                        new Point(image.height() / 2, image.width() / 2), 45, 1.0), image.size());
        Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2GRAY);

        double blurSize = 5;
//        Imgproc.GaussianBlur(image, image, new Size(blurSize, blurSize), 0);
//        Imgproc.Canny(image, image, 75, 200);
//        findContours(originalImage, image);

        Imgproc.adaptiveThreshold(image, image, 255.0, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV,
                25, 20.0);

//        Imgproc.Canny();
        double morphologySize = 3;
        int morphologyCount = 1;
        Mat morphElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                new Size(2 * morphologySize + 1, 2 * morphologySize + 1),
                new Point(morphologySize, morphologySize));
        Imgproc.morphologyEx(image, image, Imgproc.MORPH_GRADIENT, morphElement,
                new Point(morphologySize, morphologySize), morphologyCount);
        morphologySize = 2;
        morphElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                new Size(2 * morphologySize + 1, 2 * morphologySize + 1),
                new Point(morphologySize, morphologySize));
        Imgproc.erode(image, image, morphElement);
        Imgproc.erode(image, image, morphElement);

//        Mat disp_lines = extractAndAnalyzeHoughLines(image);
        Imgcodecs.imwrite("gradient.png", image);

        Mat paddedImage = new Mat();

        int height = Core.getOptimalDFTSize(image.rows());
        int width = Core.getOptimalDFTSize(image.cols());
        Core.copyMakeBorder(image, paddedImage, 0, height - image.rows(),
                0, width - image.cols(), Core.BORDER_DEFAULT);

        paddedImage.convertTo(paddedImage, CvType.CV_32F);

        Mat magn = FourierTransform.discreteFourrierTransform(paddedImage);

        Mat polar = new Mat();
        Mat rightHalf = new Mat(magn, new Range(0, magn.rows()), new Range(magn.cols() / 2, magn.cols()));
        Mat leftHalf = new Mat(magn, new Range(0, magn.rows()), new Range(0, magn.cols() / 2));
        Core.flip(leftHalf, leftHalf, -1);
        Core.add(rightHalf, leftHalf, rightHalf);
        Core.normalize(rightHalf, rightHalf, 0, 255, Core.NORM_MINMAX);

        Imgproc.logPolar(rightHalf, polar, new Point(0, rightHalf.rows() / 2), 30, Imgproc.INTER_LINEAR);
        Mat sum = new Mat();
        Core.reduce(polar, sum, 1, Core.REDUCE_SUM);
        Core.MinMaxLocResult res = Core.minMaxLoc(sum);
        Imgproc.circle(polar, res.maxLoc, 20, new Scalar(0), 10);
        double angle = (sum.rows() - res.maxLoc.y) / (sum.rows() / 2);
        angle = angle >= 1 ? angle - 1 : angle;
        angle = angle >= 0.5 ? angle - 0.5 : angle;
        System.out.println(String.format("Angle is %f*PI (%f degrees)", angle, angle * 180));

        angle = Math.toRadians(angle * 180);
        Imgproc.circle(magn, new Point(magn.cols() / 2 + magn.cols() / 2 * Math.cos(angle),
                        magn.cols() / 2 + magn.cols() / 2 * -Math.sin(angle)),
                50, new Scalar(255, 255, 255), 20);

        Mat rotated = new Mat();
        Imgproc.warpAffine(paddedImage, rotated,
                Imgproc.getRotationMatrix2D(
                        new Point(paddedImage.height() / 2, paddedImage.width() / 2), -Math.toDegrees(angle), 1.0), paddedImage.size());

        list.add(polar);
        list.add(rightHalf);
        list.add(paddedImage);
        list.add(rotated);

//        PassportProcessor passportProcessor = new PassportProcessor();
//        Mat mask = new Mat(magn.size(), magn.type());
//        Imgproc.threshold(magn, mask, 150.0, 255.0, Imgproc.THRESH_BINARY);
//        Scalar minValues = new Scalar(150);
//        Scalar maxValues = new Scalar(255);
//        Core.inRange(magn, minValues, maxValues, mask);
//        List<MatOfPoint> contours = passportProcessor.findContours(mask);
//        DrawUtils.INSTANCE.drawContours(magn, contours, true);

        Core.hconcat(list, magn);
        Imgcodecs.imwrite(out, magn);
    }

    @NotNull
    private Mat extractAndAnalyzeHoughLines(Mat image) {
        Mat lines = new Mat();
        Imgproc.HoughLinesP(image, lines, 1.5, CV_PI / 180, 200);
        Mat disp_lines = new Mat(image.size(), CvType.CV_32F, new Scalar(0, 0, 0));
        drawLinesP(disp_lines, lines);
        double newAngle = 0.;
        int nb_lines = lines.rows();
        int[] angleBar = new int[360];
        for (int i = 0; i < nb_lines; ++i) {
            double[] line = lines.get(i, 0);
            Imgproc.line(disp_lines, new Point(line[0], line[1]), new Point(line[2], line[3]),
                    new Scalar(255, 255, 255), 1);
            double theta = Math.atan2(line[3] - line[1], line[2] - line[0]);
            double angle = Math.round(Math.toDegrees(theta)) > 0 ?
                    Math.round(Math.toDegrees(theta)) :
                    -Math.round(Math.toDegrees(theta)) + 180;
            angleBar[(int) angle]++;
            newAngle += theta;
        }
        newAngle /= nb_lines; // mean angle, in radians.
        System.out.println(String.format("New angle is %f rad (%f degrees)", newAngle, newAngle * 57));
        return disp_lines;
    }

    private Size getCompressedSize(Size size) {
        boolean isHeightLonger = size.height > size.width;
        System.out.println("Old size " + size.toString());
        Size newSize = isHeightLonger ?
                new Size((maxSideLength / size.height) * size.width, maxSideLength) :
                new Size(maxSideLength, (maxSideLength / size.width) * size.height);
        System.out.println("New size " + newSize.toString());
        return newSize;
    }

    private void rotateAndBinarize(int i) {
        rotateAndBinarize(PASSPORTS_DIR + i + ".jpg", "binarized/dft" + i + ".png");
    }

    private void findContours(Mat originalImage, Mat image) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(image, contours, hierarchy, RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        for (int i = 0; i < 10; i++) {
            MatOfPoint2f convertedContour = new MatOfPoint2f(contours.get(i).toArray());
            double len = Imgproc.arcLength(convertedContour, true);
            Imgproc.approxPolyDP(convertedContour, convertedContour, 0.02 * len, true);
            drawContours(originalImage, contours, i, new Scalar(255, 0, 0), 5);
        }
    }

    private void drawLines(Mat originalImage, Mat lines) {
        Core.flip(originalImage, originalImage, 1);
        for (int i = 0; i < lines.rows(); i++) {
            double data[] = lines.get(i, 0);
            double rho1 = data[0];
            double theta1 = data[1];
            double cosTheta = Math.cos(theta1);
            double sinTheta = Math.sin(theta1);
            double x0 = cosTheta * rho1;
            double y0 = sinTheta * rho1;
            Point pt1 = new Point(x0 + 10000 * (-sinTheta), y0 + 10000 * cosTheta);
            Point pt2 = new Point(x0 - 10000 * (-sinTheta), y0 - 10000 * cosTheta);
            Imgproc.line(originalImage, pt1, pt2, new Scalar(255, 0, 0), 2);
        }
    }

    private void drawLinesP(Mat originalImage, Mat lines) {
        for (int i = 0; i < lines.rows(); i++) {
            double[] val = lines.get(i, 0);
            Imgproc.line(originalImage, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(255, 255, 255), 2);
        }
    }

}