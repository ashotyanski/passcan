package com.passcan.classifier

import mu.KotlinLogging
import org.datavec.api.io.labels.ParentPathLabelGenerator
import org.datavec.api.split.FileSplit
import org.datavec.image.loader.NativeImageLoader
import org.datavec.image.recordreader.ImageRecordReader
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator
import org.deeplearning4j.eval.Evaluation
import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.Updater
import org.deeplearning4j.nn.conf.inputs.InputType
import org.deeplearning4j.nn.conf.layers.DenseLayer
import org.deeplearning4j.nn.conf.layers.OutputLayer
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.deeplearning4j.util.ModelSerializer
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.api.DataSet
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler
import org.nd4j.linalg.lossfunctions.LossFunctions
import java.awt.image.BufferedImage
import java.io.File
import java.util.*


class NumberClassifier(restore: Boolean) {
    private val logger = KotlinLogging.logger {}
    val DATA_PATH = "data/mnist_png"

    val height = 28
    val width = 28
    val channels = 1
    val rngseed: Long = 123
    val randNumGen = Random(rngseed)
    val batchSize = 128
    val outputNum = 10
    val numEpochs = 1

    val labelList: List<Int> = listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

    // Extract the parent path as the image label
    val recordReader = ImageRecordReader(height, width, channels, ParentPathLabelGenerator())
    val scaler = ImagePreProcessingScaler(0.0, 1.0)
    val model: MultiLayerNetwork

    init {
        if (restore) {
            model = restoreModel()
        } else {
            model = createModel()
            trainModel(model)
            saveModel(model)
        }
    }

    fun createModel(): MultiLayerNetwork {
        logger.info("**** Build Model ****")
        val conf = NeuralNetConfiguration.Builder()
                .seed(rngseed)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .iterations(1)
                .learningRate(0.006)
                .updater(Updater.NESTEROVS).momentum(0.9)
                .regularization(true).l2(1e-4)
                .list()
                .layer(0, DenseLayer.Builder()
                        .nIn(height * width)
                        .nOut(100)
                        .activation(Activation.RELU)
                        .weightInit(WeightInit.XAVIER)
                        .build())
                .layer(1, OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nIn(100)
                        .nOut(outputNum)
                        .activation(Activation.SOFTMAX)
                        .weightInit(WeightInit.XAVIER)
                        .build())
                .pretrain(false).backprop(true)
                .setInputType(InputType.convolutional(height, width, channels))
                .build()

        val model = MultiLayerNetwork(conf)
        model.setListeners(ScoreIterationListener(10))

        return model
    }

    fun trainModel(model: MultiLayerNetwork) {
        logger.info("*****TRAIN MODEL********")
        val trainData = File("$DATA_PATH/training")
        val train = FileSplit(trainData, NativeImageLoader.ALLOWED_FORMATS, randNumGen)

        recordReader.initialize(train)
        val dataIter = RecordReaderDataSetIterator(recordReader, batchSize, 1, outputNum)

        // Scale pixel values to 0-1
        scaler.fit(dataIter)
        dataIter.preProcessor = scaler

        for (i in 0..numEpochs) {
            model.fit(dataIter)
        }
    }

    fun predict(img: BufferedImage) {
        val loader: NativeImageLoader = NativeImageLoader(height, width, channels)
        val image: INDArray = loader.asMatrix(img)
        scaler.transform(image)
        val output: INDArray = model.output(image)

        logger.info("## The Neural Nets Pediction ##")
        logger.info(output.toString())
        logger.info(labelList.toString());
    }

    fun predict(imgFile: File) {
        val loader: NativeImageLoader = NativeImageLoader(height, width, channels)
        val image: INDArray = loader.asMatrix(imgFile)
        scaler.transform(image)
        val output: INDArray = model.output(image)

        logger.info("## The Neural Nets Pediction ##")
        logger.info(output.toString())
        logger.info(labelList.toString());
    }

    fun testModel(model: MultiLayerNetwork) {
        logger.info("******EVALUATE MODEL******")
        val testData = File("$DATA_PATH/testing")
        val test = FileSplit(testData, NativeImageLoader.ALLOWED_FORMATS, randNumGen)

        recordReader.reset()

        // The model trained on the training dataset split
        // now that it has trained we evaluate against the
        // test data of images the network has not seen
        recordReader.initialize(test)
        val testIter = RecordReaderDataSetIterator(recordReader, batchSize, 1, outputNum)
        scaler.fit(testIter)
        testIter.preProcessor = scaler

        /*
        log the order of the labels for later use
        In previous versions the label order was consistent, but random
        In current verions label order is lexicographic
        preserving the RecordReader Labels order is no
        longer needed left in for demonstration
        purposes
        */
        logger.info(recordReader.labels.toString())

        // Create Eval object with 10 possible classes
        val eval = Evaluation(outputNum)

        // Evaluate the network
        while (testIter.hasNext()) {
            val next: DataSet = testIter.next()
            val output: INDArray = model.output(next.features)
            // Compare the Feature Matrix from the model
            // with the labels from the RecordReader
            eval.eval(next.labels, output)
        }
        logger.info(eval.stats())
    }

    fun saveModel(model: MultiLayerNetwork) {
        logger.info("******SAVE TRAINED MODEL******")
        val locationToSave: File = File("trained_mnist_model.zip")
        ModelSerializer.writeModel(model, locationToSave, false)
    }

    fun restoreModel(): MultiLayerNetwork {
        logger.info("******LOAD TRAINED MODEL******")
        val locationToSave = File("trained_mnist_model.zip")
        if (locationToSave.exists()) {
            println("\n######Saved Model Found######\n")
        } else {
            println("\n\n#######File not found!#######")
            println("This example depends on running ")
            println("MnistImagePipelineExampleSave")
            println("Run that Example First")
            println("#############################\n\n")

            System.exit(0)
        }

        return ModelSerializer.restoreMultiLayerNetwork(locationToSave)
    }
}