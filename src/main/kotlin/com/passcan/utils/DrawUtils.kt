package com.passcan.utils

import mu.KotlinLogging
import org.opencv.core.*
import org.opencv.imgproc.Imgproc


object DrawUtils {
    private val logger = KotlinLogging.logger {}

    fun drawRect(dst: Mat, rect: Rect, color: Scalar = Color.WHITE): Mat {
        Imgproc.rectangle(dst, rect.tl(), rect.br(), Scalar(255.0, 0.0, 0.0), 4)
        return dst
    }

    fun drawRotatedRect(dst: Mat, rect: RotatedRect, color: Scalar = Color.WHITE): Mat {
        val points = arrayOfNulls<Point>(4)
        rect.points(points)
        for (i in 0..3) Imgproc.line(dst, points[i], points[(i + 1) % 4], color, 10)
        Imgproc.rectangle(dst, rect.boundingRect().tl(), rect.boundingRect().br(), Scalar(255.0, 0.0, 0.0), 4)

        for (i in 0..points.size - 1)
            Imgproc.putText(dst, i.toString(), points[i], 3, 2.0, Color.YELLOW, 5)

        return dst
    }

    fun drawContours(src: Mat, contours: List<MatOfPoint>, withBounds: Boolean = false) {
//        if (hier.size().height > 0 && hier.size().width > 0) {
        // for each contour, display it in blue
        /*var i = 0
        while (i >= 0) {
            var c = MatOfPoint2f(contours[i])
            var peri = Imgproc.arcLength(c, true)
            var approx = Imgproc.approxPolyDP(c, 0.04 * peri, True)

            Imgproc.drawContours(src, contours, i, Scalar(0.0, 255.0, 0.0), 5)
            i = hier.get(0, i)[0].toInt()
        }
      }
    */
        for (contour in contours) {
            Imgproc.drawContours(src, listOf(contour), -1, Scalar(0.0, 255.0, 0.0), 5)
            if (withBounds) {
                val convertedContour = MatOfPoint2f()
                contour.convertTo(convertedContour, CvType.CV_32FC2)
                val peri = Imgproc.arcLength(convertedContour, true)

                val polygon = MatOfPoint2f()
                Imgproc.approxPolyDP(convertedContour, polygon, peri * 0.04, true)

                val convertedPolygon = MatOfPoint()
                polygon.convertTo(convertedPolygon, CvType.CV_32S)
                val boundingRotatedRect = Imgproc.minAreaRect(convertedContour)

                val points = arrayOfNulls<Point>(4)
                boundingRotatedRect.points(points)

                for (i in 0..3)
                    Imgproc.line(src, points[i], points[(i + 1) % 4], Color.YELLOW, 6)

                Imgproc.rectangle(src, boundingRotatedRect.boundingRect().tl(), boundingRotatedRect.boundingRect().br(), Scalar(255.0, 0.0, 0.0), 4)
            }
        }
    }
    /*
    fun drawLongestContour(dst: Mat, contours: List<MatOfPoint>, withPhoto: Boolean = false) {
        var longestContour: RotatedRect = RotatedRect()

        for (contour in contours) {
//            Imgproc.drawContours(dst, listOf(contour), -1, Scalar(0.0, 255.0, 0.0), 5)
            val convertedContour = MatOfPoint2f()
            contour.convertTo(convertedContour, CvType.CV_32FC2)
            val boundingRotatedRect = Imgproc.minAreaRect(convertedContour)

            if (hasLongerSide(boundingRotatedRect, longestContour)) {
                longestContour = boundingRotatedRect
            }
        }
        val points = arrayOfNulls<Point>(4)
        longestContour.points(points)
        for (i in 0..3) Imgproc.line(dst, points[i], points[(i + 1) % 4], Scalar(255.0, 255.0, 255.0), 10)
        Imgproc.rectangle(dst, longestContour.boundingRect().tl(), longestContour.boundingRect().br(), Scalar(255.0, 0.0, 0.0), 4)

        if (withPhoto) {
            var photoContour: RotatedRect = RotatedRect()

            for (contour in contours) {
                val convertedContour = MatOfPoint2f()
                contour.convertTo(convertedContour, CvType.CV_32FC2)
                val boundingRotatedRect = Imgproc.minAreaRect(convertedContour)
                if (isPhotoNearLongest(longestContour, boundingRotatedRect)) {
                    photoContour = boundingRotatedRect
                    break
                }
            }

            photoContour.points(points)
            for (i in 0..3) Imgproc.line(dst, points[i], points[(i + 1) % 4], Scalar(255.0, 255.0, 255.0), 10)
            Imgproc.rectangle(dst, photoContour.boundingRect().tl(), photoContour.boundingRect().br(), Scalar(255.0, 0.0, 0.0), 4)
        }
    }
*/
    object Color {
        val BLUE: Scalar = Scalar(0.0, 0.0, 255.0)
        val GREEN: Scalar = Scalar(0.0, 255.0, 0.0)
        val YELLOW: Scalar = Scalar(0.0, 255.0, 255.0)
        val WHITE: Scalar = Scalar(255.0, 255.0, 255.0)
    }
}