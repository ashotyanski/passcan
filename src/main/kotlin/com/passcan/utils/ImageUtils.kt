package com.passcan.utils

import javafx.embed.swing.SwingFXUtils
import javafx.scene.image.WritableImage
import mu.KotlinLogging
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Scalar
import java.awt.image.BufferedImage
import java.awt.image.DataBufferByte
import java.awt.image.DataBufferInt


object ImageUtils {
    private val logger = KotlinLogging.logger {}

    fun matToBufferedImage(original: Mat): WritableImage {
        val image: BufferedImage
        val width = original.width()
        val height = original.height()
        val channels = original.channels()
        val sourcePixels = ByteArray(width * height * channels)
        original.get(0, 0, sourcePixels)

        if (original.channels() > 1) {
            image = BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR)
        } else {
            image = BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY)
        }
        val targetPixels = (image.raster.dataBuffer as DataBufferByte).data

        System.arraycopy(sourcePixels, 0, targetPixels, 0, sourcePixels.size)

        return SwingFXUtils.toFXImage(image, null);
    }

    fun bufferedImageToMat(image: BufferedImage): Mat {
        val pixels = (image.raster.dataBuffer as DataBufferByte).data

        val type = CvType.CV_16SC1
        val mat = Mat(image.height, image.width, type)
        logger.info { "Data.length = ${pixels.size}, type = ${image.type} and $type" }
        mat.put(0, 0, pixels.copyOfRange(0, pixels.size - 0))
        return mat

    }


}
