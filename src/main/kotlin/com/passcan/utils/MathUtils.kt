package com.passcan.utils

import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.core.RotatedRect
import org.opencv.core.Size

object MathUtils {
    fun euclideanDistance(p1: Point, p2: Point): Double {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2.0) + Math.pow(p1.y - p2.y, 2.0))
    }

    fun isRelativelyEqual(d1: Double, d2: Double, deviation: Double = 0.05): Boolean {
        return Math.abs(d1 - d2) < d2 * deviation
    }

    fun rotatedRectNormalizedAngle(rotatedRect: RotatedRect): Double {
        if (rotatedRect.size.width < rotatedRect.size.height) {
            return rotatedRect.angle + 180
        } else {
            return rotatedRect.angle + 90
        }
    }

    fun isLarger(s1: RotatedRect, s2: RotatedRect): Boolean {
        return s1.size.width + s1.size.height > s2.size.width + s2.size.height
    }

    fun hasLongerSide(s1: RotatedRect, s2: RotatedRect): Boolean {
        val m1Longest = if (s1.size.width > s1.size.height)
            s1.size.width
        else
            s1.size.height

        val m2Longest = if (s2.size.width > s2.size.height)
            s2.size.width
        else
            s2.size.height

        return m1Longest > m2Longest
    }

    fun middleOfTwoPoints(p1: Point, p2: Point): Point {
        return Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2)
    }

    fun angleBetweenTwoPoints(p1: Point, p2: Point): Double {
        val len1 = Math.sqrt(p1.x * p1.x + p1.y * p1.y)
        val len2 = Math.sqrt(p2.x * p2.x + p2.y * p2.y)

        val dot = p1.dot(p2)

        val angle = dot / (len1 * len2)

        if (angle >= 1.0)
            return Math.toDegrees(0.0)
        else if (angle <= -1.0)
            return Math.toDegrees(Math.PI)
        else
            return Math.toDegrees(Math.acos(angle)) // 0..PI

    }

    fun sortedSize(size: Size): Pair<Double, Double> {
        if (size.width > size.height)
            return Pair(size.width, size.height)
        return Pair(size.height, size.width)
    }

    fun affineTransformPoint(point: Point, tranformMatrix: Mat): Point {
//       To wzrp point p1 = (x1, y1) around point (x0, y0) by angle a:
//        x2 = ((x1 - x0) * cos(a)) - ((y1 - y0) * sin(a)) + x0;
//        y2 = ((x1 - x0) * sin(a)) + ((y1 - y0) * cos(a)) + y0;
        val newPoint = Point()
        newPoint.x = tranformMatrix.get(0, 0)[0] * point.x - tranformMatrix.get(1, 0)[0] * point.y + tranformMatrix.get(0, 2)[0];
        newPoint.y = tranformMatrix.get(0, 1)[0] * point.x + tranformMatrix.get(1, 1)[0] * point.y //+ tranformMatrix.get(1, 2)[0];

        return newPoint
    }

    fun rotatePoint(around: Point, target: Point, angle: Double): Point {
        val newPoint = Point()
        val angleInRads = Math.toRadians(angle)
        newPoint.x = (target.x - around.x) * Math.cos(angleInRads) - (target.y - around.y) * Math.sin(angleInRads) + around.x
        newPoint.y = (target.x - around.x) * Math.sin(angleInRads) + (target.y - around.y) * Math.cos(angleInRads) + around.y
        return newPoint
    }
}