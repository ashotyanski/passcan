package com.passcan.passport

import com.passcan.utils.ImageUtils
import com.passcan.utils.MathUtils
import javafx.scene.image.Image
import mu.KotlinLogging
import org.opencv.core.*
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc


class PassportScanner(pathToImage: String) {
    private val logger = KotlinLogging.logger {}

    private val _passport: Passport = Passport()
    val passport: Passport
        get() {
            return _passport
        }

    val imageMatrix: Mat
    val passportProcessor: PassportProcessor = PassportProcessor()

    init {
//        val rawTrimmedImage = passportProcessor.trim(Imgcodecs.imread(pathToImage))
        val rawTrimmedImage = Imgcodecs.imread(pathToImage)
        if (rawTrimmedImage.height() < rawTrimmedImage.width()) {
            Core.flip(rawTrimmedImage.t(), rawTrimmedImage, 1)
//            Imgproc.warpAffine(rawTrimmedImage, rawTrimmedImage, Imgproc.getRotationMatrix2D(
//                    Point((rawTrimmedImage.cols() / 2).toDouble(), (rawTrimmedImage.rows() / 2).toDouble()), 90.0, 1.0)
//                    , Size((rawTrimmedImage.cols()).toDouble(), (rawTrimmedImage.rows()).toDouble()))
        }

        imageMatrix = rawTrimmedImage

        _passport.resolution = Pair(imageMatrix.height(), imageMatrix.width())
    }

    fun scan(): Passport {
        return passport
    }

    fun scanWithImage(): Image {
        // preprocess image

        var (imgMat: Mat, mask: Mat) = passportProcessor.preProcessImage(imageMatrix)
        val resultMat = imgMat
//        Core.hconcat(listOf(imgMat, mask), resultMat)

//        var imgMat: Mat = passportProcessor.trim(imageMatrix)
        // get all contours
        /*
        val contours = passportProcessor.findContours(mask)

        // get the longest contour (binding) and check correctness
        val binding = findLongest(contours)
        if (!isBindingSizeCorrect(imgMat, binding))
            logger.debug { "Found binding is incorrect: " + binding.toString() }

        // search for photo and if found, draw it and get combat ready
        val photo = findPhoto(binding, contours)
        if (photo.size == Size()) {
            logger.warn { "Photo was not found." }
            return ImageUtils.matToBufferedImage(imgMat)
        }
        DrawUtils.drawRotatedRect(imgMat, photo)
        DrawUtils.drawRotatedRect(imgMat, binding)


        val rotationMatrix = Imgproc.getRotationMatrix2D(binding.center,
                //                Point((imgMat.cols() / 2).toDouble(), (imgMat.rows() / 2).toDouble()),
                MathUtils.rotatedRectNormalizedAngle(binding) - 90, 1.0)
        Imgproc.warpAffine(imgMat, imgMat, rotationMatrix, imgMat.size())

        val oldAngle = MathUtils.rotatedRectNormalizedAngle(binding)
        val difference = Math.abs(MathUtils.rotatedRectNormalizedAngle(binding) -
                MathUtils.rotatedRectNormalizedAngle(photo))
        binding.angle = 90.0;
        val bindingSorted = MathUtils.sortedSize(binding.size)
        binding.size = Size(bindingSorted.first, bindingSorted.second)

        photo.center = MathUtils.rotatePoint(binding.center, photo.center, -oldAngle)
        photo.angle = binding.angle - difference
        val photoSorted = MathUtils.sortedSize(photo.size)
        photo.size = Size(photoSorted.first, photoSorted.second)

        //        if (photo.center.x < binding.center.x)
        //            Core.flip(imgMat.t(), imgMat, 1)

        val newArea = calculateNewArea(binding, photo)
        DrawUtils.drawRotatedRect(imgMat, newArea, color = DrawUtils.Color.WHITE)

        val roi = intersectingRegion(imgMat, newArea.boundingRect())
        DrawUtils.drawRect(imgMat, roi, color = DrawUtils.Color.GREEN)

        //        val resultMat = imgMat.submat(roi)
        //        Imgproc.getRectSubPix(imgMat, rotatedNewArea.size(), newArea.center, resultMat)
        //        passportProcessor.drawLongestContour(imgMat, contours, true)
        //        passportProcessor.drawContours(imgMat, contours, true)
        */
        return ImageUtils.matToBufferedImage(resultMat)
    }

    private fun intersectingRegion(mat: Mat, roi: Rect): Rect {
        return Rect(
                if (roi.x < 0) 0 else roi.x + 1,
                if (roi.y < 0) 0 else roi.y + 1,
                if (roi.br().x >= mat.cols()) mat.cols() - roi.x - 1 else roi.width - 1,
                if (roi.br().y >= mat.rows()) mat.rows() - roi.y - 1 else roi.height - 1
        )
    }

    private fun calculateNewArea(binding: RotatedRect, photo: RotatedRect): RotatedRect {
/*
        val height = if (binding.size.width < binding.size.height) binding.size.width else binding.size.height
        val initialPoints = binding.pointsArray()
        var sidePoint1 = Point()
        var sidePoint2 = Point()
        if (MeasuringUtils.isRelativelyEqual(MeasuringUtils.euclideanDistance(initialPoints[0]!!, initialPoints[1]!!), height)) {
            sidePoint1 = MeasuringUtils.middleOfTwoPoints(initialPoints[0]!!, initialPoints[1]!!)
            sidePoint2 = MeasuringUtils.middleOfTwoPoints(initialPoints[2]!!, initialPoints[3]!!)
        } else {
            sidePoint1 = MeasuringUtils.middleOfTwoPoints(initialPoints[0]!!, initialPoints[3]!!)
            sidePoint2 = MeasuringUtils.middleOfTwoPoints(initialPoints[1]!!, initialPoints[2]!!)
        }
*/

//        val width = if (binding.size.width == height) binding.size.height else binding.size.width


        val heightToWidthRatio = 0.7

        return RotatedRect(Point(binding.center.x - binding.size.height * 0.2, binding.center.y - binding.size.height * 0.5)
                , Size(binding.size.width * heightToWidthRatio * 2, binding.size.width + binding.size.height * 0.4)
                , binding.angle)
        /*
        return RotatedRect(Point(binding.center.x - binding.size.height, binding.center.y + binding.size.height * 0.2)
                , Size(binding.size.width + binding.size.height * 0.4, binding.size.width * heightToWidthRatio * 2)
                , binding.angle)*/

/*        if (binding.size.width > binding.size.height) {
//            val newHeight = heightToWidthRatio * width
            return RotatedRect(binding.center
                    , Size(binding.size.width, binding.size.width * heightToWidthRatio * 2)
                    , binding.angle)
        } else {
            return RotatedRect(binding.center
                    , Size(binding.size.height * heightToWidthRatio * 2, binding.size.height)
                    , binding.angle)
        }
*/
    }

    private fun findLongest(contours: List<MatOfPoint>): RotatedRect {
        var longestContour: RotatedRect = RotatedRect()

        for (contour in contours) {
            val convertedContour = MatOfPoint2f()
            contour.convertTo(convertedContour, CvType.CV_32FC2)
            val boundingRotatedRect = Imgproc.minAreaRect(convertedContour)

            if (MathUtils.hasLongerSide(boundingRotatedRect, longestContour)) {
                longestContour = boundingRotatedRect
            }
        }
        return longestContour
    }

    private fun findPhoto(binding: RotatedRect, contours: List<MatOfPoint>): RotatedRect {
        var photo: RotatedRect = RotatedRect()

        for (contour in contours) {
            val convertedContour = MatOfPoint2f()
            contour.convertTo(convertedContour, CvType.CV_32FC2)
            val boundingRotatedRect = Imgproc.minAreaRect(convertedContour)
            if (isPhoto(binding, boundingRotatedRect)) {
                photo = boundingRotatedRect
                break
            }
        }
        return photo
    }

    fun isBindingSizeCorrect(imageMatrix: Mat, longestRect: RotatedRect): Boolean {
        val bindHeightToWidth = 18.0
        val size = MathUtils.sortedSize(longestRect.size);
        return MathUtils.isRelativelyEqual(size.first / size.second, bindHeightToWidth, 0.25)
    }

    fun isPhoto(longestRect: RotatedRect, rect: RotatedRect): Boolean {
        // check if square
        if (!MathUtils.isRelativelyEqual(rect.size.height, rect.size.width))
            return false

        // check if both's angle's are equal
        val angle1 = MathUtils.rotatedRectNormalizedAngle(rect)
        val angle2 = if (angle1 >= 90) angle1 - 90 else angle1 + 90
        val angle3 = MathUtils.rotatedRectNormalizedAngle(longestRect)
        if ((angle1 > angle3 + 0.55 || angle1 < angle3 - 0.55)
                && (angle2 > angle3 + 0.55 || angle2 < angle3 - 0.55))
            return false

        // check size
        if (!isPhotoCorrectSize(longestRect, rect)) {
            return false
        }
        // check position
        if (!isPhotoCorrectPosition(longestRect, rect))
            return false

        logger.debug { rect.toString() + ",  " + longestRect.toString() }
        return true
    }

    fun isPhotoCorrectSize(longestRect: RotatedRect, rect: RotatedRect): Boolean {
        val photoToBindRatio = 0.3158
        val expectedSize = MathUtils.sortedSize(longestRect.size).first * photoToBindRatio

        val angle = MathUtils.rotatedRectNormalizedAngle(rect)
        val actualSize = if (angle > 45 && angle < 135)
            rect.size.width
        else
            rect.size.height

        if (MathUtils.isRelativelyEqual(actualSize, expectedSize))
            return true


        logger.debug {
            "Last photo check failed: " +
                    "expected: ${longestRect.size}(${expectedSize.toInt()})" +
                    ", got: ${rect.size}(${actualSize.toInt()}"
        }
        return false
    }

    fun isPhotoCorrectPosition(longestRect: RotatedRect, rect: RotatedRect): Boolean {
        // check radius
        val radiusRatio = 0.8
        val expectedRadius = MathUtils.sortedSize(longestRect.size).first / 2 * radiusRatio

        val actualRadius = MathUtils.euclideanDistance(longestRect.center, rect.center)
        if (!MathUtils.isRelativelyEqual(actualRadius, expectedRadius))
            return false

        // check angle
        val points = longestRect.pointsArray()
        var nearestPoint: Point = points[0]
        var nearestToNearest: Point = points[0]

        for (i in 0..3)
            if (MathUtils.euclideanDistance(rect.center, points[i]) <= MathUtils.euclideanDistance(rect.center, nearestPoint)) {
                nearestPoint = points[i]
                val previousPointDist = MathUtils.euclideanDistance(points[(i + 1).rem(4)], points[i])
                val nextPointDist = MathUtils.euclideanDistance(points[(i + 3).rem(4)], points[i])

                if (previousPointDist < nextPointDist)
                    nearestToNearest = points[(i + 1).rem(4)]
                else
                    nearestToNearest = points[(i + 1).rem(4)]
            }

        val sideMiddlePoint = MathUtils.middleOfTwoPoints(nearestPoint, nearestToNearest)
        val expectedAngle = 39.0
        val actualAngle = MathUtils.angleBetweenTwoPoints(
                Point(sideMiddlePoint.x - longestRect.center.x, sideMiddlePoint.y - longestRect.center.y),
                Point(rect.center.x - longestRect.center.x, rect.center.y - longestRect.center.y)
        )
        if (actualAngle < expectedAngle * 0.95 || actualAngle > expectedAngle * 1.05)
            return false


        return true
    }
}