package com.passcan.passport

import com.passcan.controllers.ProcessingValues
import com.passcan.controllers.ProcessingValues.blurSize
import com.passcan.controllers.ProcessingValues.dilateCount
import com.passcan.controllers.ProcessingValues.dilateSize
import com.passcan.controllers.ProcessingValues.dilateType
import com.passcan.controllers.ProcessingValues.erodeCount
import com.passcan.controllers.ProcessingValues.erodeSize
import com.passcan.controllers.ProcessingValues.hueStart
import com.passcan.controllers.ProcessingValues.hueStop
import com.passcan.controllers.ProcessingValues.morphologyCount
import com.passcan.controllers.ProcessingValues.morphologySize
import com.passcan.controllers.ProcessingValues.morphologyType
import com.passcan.controllers.ProcessingValues.saturationStart
import com.passcan.controllers.ProcessingValues.saturationStop
import com.passcan.controllers.ProcessingValues.valueStart
import com.passcan.controllers.ProcessingValues.valueStop
import com.passcan.utils.DrawUtils
import com.passcan.utils.MathUtils.isLarger
import mu.KotlinLogging
import org.opencv.core.*
import org.opencv.imgproc.Imgproc


class PassportProcessor {
    private val logger = KotlinLogging.logger {}

    //Hue = [0.0-180.0], Saturation = [20.0;255.0], Value = [0.0;255.0], Dilate = 14.0, Erode = 5.0, Blur = 7.0

    // {"blurSize":0,"dilateType":0,"morphologyType":4,"valueStop":0,"dilateSize":5,"morphologySize":6,"erodeCount":2,"valueStart":0,"saturationStop":255,"erodeSize":8.095238095238095,"dilateCount":2,"saturationStart":58.64285714285714,"morphologyCount":4,"hueStart":0,"hueStop":0}

    fun trim(imageMatrix: Mat): Mat {
        val rawMat = imageMatrix.clone()
        var imgMat = rawMat
        val mask = Mat()

//        val minValues = Scalar(hue.first, saturation.first, value.first)
//        val maxValues = Scalar(hue.second, saturation.second, value.second)
//        val minValues = Scalar(0.0, 20.0, 0.0)
//        val maxValues = Scalar(180.0, 255.0, 255.0)
//        Core.inRange(imgMat, minValues, maxValues, mask)

        Imgproc.cvtColor(imgMat, imgMat, Imgproc.COLOR_BGR2GRAY)
        val distance = -1
        val blurSize = 30
        if (blurSize > 0) {
            val tmp = Mat()
            Imgproc.bilateralFilter(imgMat, tmp, distance, (blurSize * 2).toDouble(), (blurSize * 2).toDouble())
            imgMat = tmp
        }
/*
{"blurSize":60,"dilateType":0,"morphologyType":4,"valueStop":0,"dilateSize":2,"morphologySize":14,"erodeCount":1,"valueStart":0,"saturationStop":255,"erodeSize":0,"dilateCount":1,"saturationStart":45,"morphologyCount":4,"hueStart":0,"hueStop":0}

        val distance = -1
        val blurSize = 30
        if (blurSize > 0) {
            val tmp = Mat()
            Imgproc.bilateralFilter(imgMat, tmp, distance, (blurSize * 2).toDouble(), (blurSize*2).toDouble())
            imgMat = tmp
        }

        val dilateSize = 5.0
        val dilateCount = 2
        val dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                Size(2 * dilateSize + 1, 2 * dilateSize + 1))
        Imgproc.dilate(imgMat, imgMat, dilateElement, Point(dilateSize, dilateSize), dilateCount)

        val erodeSize = 8.0
        val erodeCount = 2
        val erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                Size(2 * erodeSize + 1, 2 * erodeSize + 1))
        Imgproc.erode(imgMat, imgMat, erodeElement, Point(erodeSize, erodeSize), erodeCount)

        val morphologySize = 6.0
        val morphologyCount = 4
        val morphElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                Size(2 * morphologySize + 1, 2 * morphologySize + 1), Point(morphologySize, morphologySize))
        Imgproc.morphologyEx(imgMat, imgMat, Imgproc.MORPH_GRADIENT, morphElement,
                Point(morphologySize, morphologySize), morphologyCount)

        Imgproc.threshold(imgMat, mask, 60.0, 255.0, Imgproc.THRESH_BINARY)
*/

        if (dilateSize > 0) {
            val dilateElement = Imgproc.getStructuringElement(getMorphElem(dilateType), Size(2 * dilateSize + 1, 2 * dilateSize + 1))
            Imgproc.dilate(imgMat, imgMat, dilateElement, Point(dilateSize, dilateSize), dilateCount)
        }
        if (erodeSize > 0) {
            val erodeElement = Imgproc.getStructuringElement(getMorphElem(dilateType), Size(2 * erodeSize + 1, 2 * erodeSize + 1))
            Imgproc.erode(imgMat, imgMat, erodeElement, Point(erodeSize, erodeSize), erodeCount)
        }

        if (morphologySize > 0) {
            val morphElement = Imgproc.getStructuringElement(getMorphElem(dilateType),
                    Size(2 * morphologySize + 1, 2 * morphologySize + 1), Point(morphologySize, morphologySize))
            Imgproc.morphologyEx(imgMat, imgMat, getMorphOperator(morphologyType), morphElement,
                    Point(morphologySize, morphologySize), morphologyCount)
        }

        Imgproc.threshold(imgMat, mask, saturationStart, saturationStop, Imgproc.THRESH_BINARY)

        val contours = findContours(mask)
        val largestRect = extractLargestRect(contours)
        DrawUtils.drawRotatedRect(imgMat, largestRect)

        val resultMat = Mat()
        Core.hconcat(listOf(imgMat, mask), resultMat)
//        Imgproc.warpAffine(rawMat, rawMat,
//                Imgproc.getRotationMatrix2D(largestRect.center, largestRect.angle, 1.0), rawMat.size())
//        Imgproc.getRectSubPix(resultMat, largestRect.size, largestRect.center, resultMat)
//        drawRotatedRect(resultMat, largestRect, color = ImageUtils.Color.BLUE)

//        if (largestRect.angle <= -45) Core.flip(resultMat.t(), resultMat, 0)
//        val dst = src.submat(largestRect.boundingRect())
        return resultMat
    }

    fun preProcessImage(imageMatrix: Mat): Pair<Mat, Mat> {
        val rawMat = imageMatrix.clone()
        var imgMat = rawMat
        val mask = Mat()

        Imgproc.cvtColor(imgMat, imgMat, Imgproc.COLOR_BGR2HSV)
/*
        pass 1,4 / bind strong, photo strong
        Hue = [173.0-180.0], Saturation = [66.0;255.0], Value = [70.0;255.0], Dilate = 21.0, Erode = 1.0, Blur = 10.0

        pass 1,2,3,4 / bing strong, photo strong
        Hue = [142.0-180.0], Saturation = [70.0;255.0], Value = [203.7857142857143;255.0], Dilate = 12.5, Erode = 5.5, Blur = 5.0
*/
        val distance = -1
//        val blurSize = 30
        if (blurSize > 0) {
//            val tmp = Mat()
//            Imgproc.bilateralFilter(imgMat, tmp, distance, blurSize.toDouble(), blurSize.toDouble())
//            imgMat = tmp
        }

        if (ProcessingValues.dilateSize > 0) {
            val dilateElement = Imgproc.getStructuringElement(getMorphElem(dilateType),
                    Size(2 * ProcessingValues.dilateSize + 1, 2 * ProcessingValues.dilateSize + 1))
            Imgproc.dilate(imgMat, imgMat, dilateElement, Point(ProcessingValues.dilateSize, ProcessingValues.dilateSize), dilateCount)
        }
        if (ProcessingValues.erodeSize > 0) {
            val erodeElement = Imgproc.getStructuringElement(getMorphElem(dilateType),
                    Size(2 * ProcessingValues.erodeSize + 1, 2 * ProcessingValues.erodeSize + 1))
            Imgproc.erode(imgMat, imgMat, erodeElement, Point(ProcessingValues.erodeSize, ProcessingValues.erodeSize), erodeCount)
        }

        if (morphologySize > 0) {
            val morphElement = Imgproc.getStructuringElement(getMorphElem(dilateType),
                    Size(2 * morphologySize + 1, 2 * morphologySize + 1), Point(morphologySize, morphologySize))
            Imgproc.morphologyEx(imgMat, imgMat, getMorphOperator(morphologyType), morphElement,
                    Point(morphologySize, morphologySize), morphologyCount)
        }

        val minValues = Scalar(hueStart, saturationStart, valueStart)
        val maxValues = Scalar(hueStop, saturationStop, valueStop)

        Core.inRange(imgMat, minValues, maxValues, mask)
//        Imgproc.threshold(imgMat, mask, 190.0, 230.0, Imgproc.THRESH_BINARY)

        val contours = findContours(mask)
        val result = extractLargestRect(contours)
        DrawUtils.drawRotatedRect(imgMat, result, DrawUtils.Color.GREEN)
//        drawContours(imgMat, contours, true)

        Imgproc.cvtColor(mask, mask, Imgproc.COLOR_GRAY2BGR)

        val resultMat = Mat()
        Core.hconcat(listOf(imgMat, mask), resultMat)

        return Pair(resultMat, mask)
    }

    fun findContours(src: Mat): List<MatOfPoint> {
        val contours = ArrayList<MatOfPoint>()
        val hier = Mat()

        val converted: Mat = Mat();
        src.convertTo(converted, CvType.CV_32SC1)
//        println("" + (converted.type()) + " and cvshit " + CvType.CV_32SC1)

        Imgproc.findContours(src, contours, hier, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE)
        //logger.info { "Contour count " + contours.size }
        return contours
    }

    fun extractLargestRect(contours: List<MatOfPoint>): RotatedRect {
        var largestContour: RotatedRect = RotatedRect()
        for (contour in contours) {
            val convertedContour = MatOfPoint2f()
            contour.convertTo(convertedContour, CvType.CV_32FC2)
            val boundingRotatedRect = Imgproc.minAreaRect(convertedContour)

            if (isLarger(boundingRotatedRect, largestContour)) {
                largestContour = boundingRotatedRect
            }
        }
        return largestContour
    }

    private fun getMorphElem(i: Int): Int {
        return when (i) {
            0 -> Imgproc.MORPH_RECT
            1 -> Imgproc.MORPH_CROSS
            2 -> Imgproc.MORPH_ELLIPSE
            else -> Imgproc.MORPH_RECT
        }
    }

    private fun getMorphOperator(i: Int): Int {
        return when (i) {
            2 -> Imgproc.MORPH_OPEN
            3 -> Imgproc.MORPH_CLOSE
            4 -> Imgproc.MORPH_GRADIENT
            5 -> Imgproc.MORPH_TOPHAT
            6 -> Imgproc.MORPH_BLACKHAT
            else -> Imgproc.MORPH_OPEN
        }
    }
}

fun RotatedRect.pointsArray(): Array<Point> {
    val points = arrayOf(Point(), Point(), Point(), Point())
    this.points(points)
    return points
}