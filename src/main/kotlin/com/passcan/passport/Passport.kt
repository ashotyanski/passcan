package com.passcan.passport

import javafx.scene.image.Image
import java.util.*

class Passport {
    enum class Gender {
        NotSpecified, Male, Female
    }

    var name: String = ""
    var surname: String = ""
    var birthDate: Date = Date()
    var id: Int = 0
    var series: Int = 0
    var city: String = ""
    var gender: Gender = Gender.NotSpecified
    var photo: Image? = null
    var resolution: Pair<Int, Int>? = null
}