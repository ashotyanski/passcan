package com.passcan

import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc

class StartBinarization {

    private fun run() {
        val image = Imgcodecs.imread(PASSPORTS_DIR + "6.jpg")

        Imgproc.warpAffine(image, image,
                Imgproc.getRotationMatrix2D(
                        Point((image.height() / 2).toDouble(), (image.width() / 2).toDouble()), 1.56, 1.0), image.size())
        var longest_side = if (image.cols() > image.rows()) image.rows() else image.cols()
        longest_side /= 10
        val block_size = if (longest_side % 2 == 1) longest_side else longest_side - 1

        Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2GRAY)

        var xDerivs = Mat()
        var xAbsDerivs = Mat()
        var yDerivs = Mat()
        var yAbsDerivs = Mat()
        Imgproc.Sobel(image, xDerivs, CvType.CV_16S, 1, 0, 5, 1.0, 0.0)
        Imgproc.Sobel(image, yDerivs, CvType.CV_16S, 0, 1, 5, 1.0, 0.0)
        Core.convertScaleAbs(xDerivs, xAbsDerivs)
        Core.convertScaleAbs(yDerivs, yAbsDerivs)
        Core.addWeighted(xAbsDerivs, 0.5, yAbsDerivs, 0.5, 0.0, xDerivs)

        var binarized = Mat()
        Imgproc.adaptiveThreshold(image, binarized, 255.0, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY,
                block_size, 5.0)

        val list = mutableListOf<Mat>()
        list.add(image)
        list.add(binarized)
        Core.hconcat(list, image)
        Imgcodecs.imwrite("binarized.png", image)
    }

    companion object {
        private val PASSPORTS_DIR = "src/main/resources/passcan/img/pass/"

        @JvmStatic
        fun main(args: Array<String>) {
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
            StartBinarization().run()
        }
    }
}