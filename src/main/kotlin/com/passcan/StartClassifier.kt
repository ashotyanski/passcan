package com.passcan

import javafx.application.Application
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.layout.StackPane
import javafx.stage.Stage
import org.opencv.core.Core

fun main(args: Array<String>) {
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    StartClassifier().run(args)
}

class StartClassifier : Application() {
    override fun start(primaryStage: Stage) {

        val loader = FXMLLoader(this::class.java.getResource("/passcan/fxml/ClassifierLayout.fxml"))
        val root = loader.load<StackPane>()
        val scene = Scene(root)
        primaryStage.onCloseRequest = EventHandler {
            Platform.exit()
            System.exit(0)
        }

        primaryStage.scene = scene
        primaryStage.show()
    }

    fun run(args: Array<String>) {
        Application.launch(*args)
    }
}
