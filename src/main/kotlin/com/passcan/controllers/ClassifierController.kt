package com.passcan.controllers

import com.passcan.classifier.NumberClassifier
import javafx.embed.swing.SwingFXUtils
import javafx.fxml.FXML
import javafx.scene.SnapshotParameters
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.image.Image
import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO.write


class ClassifierController {

    @FXML
    lateinit var canvas: Canvas

    private val classifier = NumberClassifier(true)

    @FXML
    fun initialize() {
        val graphicsContext = canvas.graphicsContext2D
        initDraw(graphicsContext)

        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED) { event ->
            //  graphicsContext.clearRect(0.0, 0.0, canvas.width, canvas.height)
            graphicsContext.fill = Color.BLACK;
            graphicsContext.fillRect(0.0, 0.0, canvas.width, canvas.height);

            graphicsContext.beginPath()
            graphicsContext.moveTo(event.x, event.y)
            graphicsContext.stroke()
        }

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED) { event ->
            graphicsContext.lineTo(event.x, event.y)
            graphicsContext.stroke()
        }

        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED) {
            val image: Image = canvas.snapshot(SnapshotParameters(),
                    SwingFXUtils.toFXImage(BufferedImage(400, 400, BufferedImage.TYPE_BYTE_BINARY), null));
            val bufferedImage = SwingFXUtils.fromFXImage(image, null)

            val newBufferedImage = bufferedImage.getScaledInstance(28, 28, java.awt.Image.SCALE_SMOOTH)
            val bimage = BufferedImage(newBufferedImage.getWidth(null), newBufferedImage.getHeight(null),
                    BufferedImage.TYPE_INT_ARGB)
            // Draw the image on to the buffered image
            val bGr = bimage.createGraphics()
            bGr.drawImage(newBufferedImage, 0, 0, null)
            bGr.dispose()

//            val mat = ImageUtils.bufferedImageToMat(bufferedImage)
//            val mat2 = Mat()
//            Imgproc.cvtColor(mat, mat2, Imgproc.COLOR_BGR2GRAY)
//            classifier.predict(SwingFXUtils.fromFXImage(ImageUtils.matToBufferedImage(mat), null))
            write(bimage, "png", File("test.png"));
//            classifier.predict(File("test.png"))
            classifier.predict(bimage)
        }
    }

    private fun initDraw(gc: GraphicsContext) {
        val canvasWidth = gc.canvas.width
        val canvasHeight = gc.canvas.height

        gc.fill = Color.LIGHTGRAY
        gc.stroke = Color.BLACK
        gc.lineWidth = 5.0

        gc.fill()
        gc.strokeRect(
                0.0, //x of the upper left corner
                0.0, //y of the upper left corner
                canvasWidth, //width of the rectangle
                canvasHeight) //height of the rectangle

        gc.fill = Color.RED
        gc.stroke = Color.WHITE
        gc.lineWidth = 17.0
    }
}
