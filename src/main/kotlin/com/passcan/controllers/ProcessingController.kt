package com.passcan.controllers

import com.passcan.passport.PassportScanner
import javafx.application.Platform
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.fxml.FXML
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.GridPane
import mu.KotlinLogging
import java.io.FileInputStream
import java.util.concurrent.*


class ProcessingController {
    private val logger = KotlinLogging.logger {}

    @FXML
    lateinit var startButton: Button
    @FXML
    lateinit var pane: GridPane

    @FXML
    lateinit var blur: Slider
    @FXML
    lateinit var morphology: Slider
    @FXML
    lateinit var morphologyType: Spinner<Int>
    @FXML
    lateinit var morphologyCount: Spinner<Int>
    @FXML
    lateinit var dilate: Slider
    @FXML
    lateinit var dilateCount: Spinner<Int>
    @FXML
    lateinit var dilateType: Spinner<Int>
    @FXML
    lateinit var erode: Slider
    @FXML
    lateinit var erodeCount: Spinner<Int>
    @FXML
    lateinit var hueStart: Slider
    @FXML
    lateinit var hueStop: Slider
    @FXML
    lateinit var saturationStart: Slider
    @FXML
    lateinit var saturationStop: Slider
    @FXML
    lateinit var valueStart: Slider
    @FXML
    lateinit var valueStop: Slider
    @FXML
    lateinit var hsvCurrentValues: TextField
    @FXML
    lateinit var resolution: Label

    var hsvValuesProp: ObjectProperty<String>? = null

    val GRID_ROW_WIDTH: Double = 300.0

    val PASSPORTS_DIR = "src/main/resources/passcan/img/pass/"
    val scanners = listOf<PassportScanner>(
            //            PassportScanner(PASSPORTS_DIR + "1.jpg"),
//            PassportScanner(PASSPORTS_DIR + "2.jpg"),
//            PassportScanner(PASSPORTS_DIR + "3.jpg"),
//            PassportScanner(PASSPORTS_DIR + "4.jpg"),
//            PassportScanner(PASSPORTS_DIR + "5.jpg"),
//            PassportScanner(PASSPORTS_DIR + "6.jpg"),
//            PassportScanner(PASSPORTS_DIR + "7.jpg"),
//            PassportScanner(PASSPORTS_DIR + "8.jpg"),
//            PassportScanner(PASSPORTS_DIR + "9.jpg"),
            PassportScanner(PASSPORTS_DIR + "10.jpg")
    )
    lateinit var pool: ExecutorService
    var latch = CountDownLatch(0)

    @FXML
    fun initialize() {
        valueStart.valueProperty().addListener { _, _, new -> ProcessingValues.valueStart = new as Double }
        valueStop.valueProperty().addListener { _, _, new -> ProcessingValues.valueStop = new as Double }
        saturationStart.valueProperty().addListener { _, _, new -> ProcessingValues.saturationStart = new as Double }
        saturationStop.valueProperty().addListener { _, _, new -> ProcessingValues.saturationStop = new as Double }
        hueStart.valueProperty().addListener { _, _, new -> ProcessingValues.hueStart = new as Double }
        hueStop.valueProperty().addListener { _, _, new -> ProcessingValues.hueStop = new as Double }
        blur.valueProperty().addListener { _, _, new -> ProcessingValues.blurSize = new.toInt() }
        erode.valueProperty().addListener { _, _, new -> ProcessingValues.erodeSize = new as Double }
        erodeCount.valueProperty().addListener { _, _, new -> ProcessingValues.erodeCount = new as Int }
        dilate.valueProperty().addListener { _, _, new -> ProcessingValues.dilateSize = new as Double }
        dilateCount.valueProperty().addListener { _, _, new -> ProcessingValues.dilateCount = new as Int }
        dilateType.valueProperty().addListener { _, _, new -> ProcessingValues.dilateType = new as Int }
        morphology.valueProperty().addListener { _, _, new -> ProcessingValues.morphologySize = new as Double }
        morphologyCount.valueProperty().addListener { _, _, new -> ProcessingValues.morphologyCount = new as Int }
        morphologyType.valueProperty().addListener { _, _, new -> ProcessingValues.morphologyType = new as Int }

        var k = 0
        for (i in 1..3)
            for (j in 1..2) {
                k++
                if (k <= scanners.size) {
                    val imageView = ImageView(Image(FileInputStream("src/main/resources/passcan/ui/opencv-logo-white.png")))
                    imageView.fitHeight = (GRID_ROW_WIDTH / imageView.image.width) * imageView.image.height
                    imageView.fitWidth = GRID_ROW_WIDTH
                    pane.add(imageView, i, j)
                    resolution.text = resolution.text + "\n " +
                            (k) + " = " + scanners[k - 1].passport.resolution?.first +
                            "*" + scanners[k - 1].passport.resolution?.second
                } else break
            }
        hsvValuesProp = SimpleObjectProperty()
        this.hsvCurrentValues.textProperty()?.bind(hsvValuesProp)

        startScheduledProcessing()
//        startProcessingOnChange()
    }

    private fun startScheduledProcessing() {
        pool = Executors.newScheduledThreadPool(scanners.size)!!
        for (i in 0 until scanners.size) {
            (pool as ScheduledExecutorService).scheduleWithFixedDelay({ updateImage() }, 300L, 300L, TimeUnit.MILLISECONDS)
        }
        (pool as ScheduledExecutorService).scheduleAtFixedRate({
            onFXThread(hsvValuesProp!!, org.json.JSONObject(ProcessingValues).toString()
            )
        }, 100L, 100L, TimeUnit.MILLISECONDS)
    }


    fun startProcessingOnChange() {
        pool = Executors.newWorkStealingPool(scanners.size)!!
        valueStart.valueProperty().addListener { _ -> refreshImagesOnce() }
        valueStop.valueProperty().addListener { _ -> refreshImagesOnce() }
        saturationStart.valueProperty().addListener { _ -> refreshImagesOnce() }
        saturationStop.valueProperty().addListener { _ -> refreshImagesOnce() }
        hueStart.valueProperty().addListener { _ -> refreshImagesOnce() }
        hueStop.valueProperty().addListener { _ -> refreshImagesOnce() }
        dilate.valueProperty().addListener { _ -> refreshImagesOnce() }
        erode.valueProperty().addListener { _ -> refreshImagesOnce() }
        blur.valueProperty().addListener { _ -> refreshImagesOnce() }
        dilateCount.valueProperty().addListener { _ -> refreshImagesOnce() }
    }

    private fun refreshImagesOnce() {
        latch.await()
        logger.info("Refresh request")
        latch = CountDownLatch(scanners.size)
        for (i in 0 until scanners.size) {
            pool.execute {
                updateImage()
                latch.countDown()
            }
        }
        pool.execute {
            onFXThread(hsvValuesProp!!, org.json.JSONObject(ProcessingValues).toString())
        }
        latch.await()
        logger.info("Done")
    }

    @FXML
    fun startProcessing() {
        hsvValuesProp = SimpleObjectProperty()
        this.hsvCurrentValues.textProperty()?.bind(hsvValuesProp)

//        val frameGrabber = Runnable {
//            updateImage()
//            onFXThread(hsvValuesProp!!, (
//                    "Hue = [" + hueStart?.value!! + "-" + hueStop?.value!! +
//                            "], Saturation = [" + saturationStart?.value!! + ";" + saturationStop?.value!! +
//                            "], Value = [" + valueStart?.value!! + ";" + valueStop?.value!! +
//                            "], Dilate = " + dilate?.value!! +
//                            ", Erode = " + erode?.value!! +
//                            ", Blur = " + blur?.value!!
//                    )
//            )
//        }
//        val pool = Executors.newSingleThreadScheduledExecutor()
//        pool.scheduleAtFixedRate(frameGrabber, 0, 1000, TimeUnit.MILLISECONDS)
    }

    private fun updateImage() {
        for (i in 0..(scanners.size - 1)) {
            (pane.children!![i] as ImageView).image = scanners[i].scanWithImage()
        }
    }

    private fun <T> onFXThread(property: ObjectProperty<T>, value: T) {
        Platform.runLater { property.set(value) }
    }
}

object ProcessingValues {
    var valueStart: Double = 0.0
    var valueStop: Double = 0.0
    var saturationStart: Double = 0.0
    var saturationStop: Double = 0.0
    var hueStart: Double = 0.0
    var hueStop: Double = 0.0
    var blurSize: Int = 0
    var erodeSize: Double = 0.0
    var erodeCount: Int = 1
    var dilateSize: Double = 0.0
    var dilateCount: Int = 1
    var dilateType: Int = 0
    var morphologySize: Double = 0.0
    var morphologyType: Int = 2
    var morphologyCount: Int = 1
}
