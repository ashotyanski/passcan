package com.fft

import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import java.util.*

class FourierTransform {
    companion object {
        @JvmStatic fun discreteFourrierTransform(paddedImage: Mat): Mat {
            val list = ArrayList<Mat>()
            list.add(paddedImage)
            list.add(Mat.zeros(paddedImage.size(), CvType.CV_32F))
            val complexImage = Mat()
            Core.merge(list, complexImage)

//            complexImage.convertTo(complexImage, CvType.CV_64FC2)

            Core.dft(complexImage, complexImage)
            Core.split(complexImage, list)
            Core.magnitude(list[0], list[1], list[0])
            var magn = list[0]

            Core.add(magn, Scalar(1.0), magn)
            Core.log(magn, magn)
            Imgproc.resize(magn, magn, Size(magn.rows().toDouble(), magn.rows().toDouble()))
            Core.normalize(magn, magn, 0.0, 255.0, Core.NORM_MINMAX)

            println(magn.size())

            magn = Mat(magn, Rect(0, 0, magn.cols() and -2, magn.rows() and -2))
            val cx = magn.cols() / 2
            val cy = magn.rows() / 2

            val q0 = Mat(magn, Rect(0, 0, cx, cy))
            val q1 = Mat(magn, Rect(cx, 0, cx, cy))
            val q2 = Mat(magn, Rect(0, cy, cx, cy))
            val q3 = Mat(magn, Rect(cx, cy, cx, cy))
            val tmp = Mat(q0.size(), q0.type())
            q0.copyTo(tmp)
            q3.copyTo(q0)
            tmp.copyTo(q3)
            q1.copyTo(tmp)
            q2.copyTo(q1)
            tmp.copyTo(q2)

            return magn
        }
    }
}