package com.fft

import com.passcan.utils.ImageUtils
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.Slider
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.AnchorPane
import javafx.stage.FileChooser
import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import java.io.File
import java.io.FileInputStream


class FftController {
    @FXML
    lateinit var pane: AnchorPane
    @FXML
    lateinit var originalImage: ImageView
    @FXML
    lateinit var transformedImage: ImageView
    @FXML
    lateinit var transformButton: Button
    @FXML
    lateinit var fileChooseButton: Button
    @FXML
    lateinit var angleSlider: Slider
    @FXML
    lateinit var scaleSlider: Slider

    private val fileChooser = FileChooser()

    private var currentImage: String = "src/main/resources/passcan/img/pass/14.jpg"
    private val PASSPORTS_DIR = "src/main/resources/passcan/img/pass/"

    @FXML
    fun initialize() {
        fileChooseButton.onAction = EventHandler {
            fileChooser.initialDirectory = File(PASSPORTS_DIR)
            val file = fileChooser.showOpenDialog(pane.scene.window)
            if (file != null) {
                currentImage = file.absolutePath
                try {
                    originalImage.image = Image(FileInputStream(file))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        transformButton.onAction = EventHandler {
            val origImage = Imgcodecs.imread(currentImage)
//            origImage.convertTo(origImage, CvType.CV_8UC3)
//            val scale = scale(origImage, scaleSlider.value)
            val warped = warp(origImage, angleSlider.value, scaleSlider.value)
            originalImage.image = ImageUtils.matToBufferedImage(warped)
            val transformed = transform(warped)
            transformed.convertTo(transformed, CvType.CV_8UC3)
            transformedImage.image = ImageUtils.matToBufferedImage(transformed)
        }
    }

    fun scale(image: Mat, scale: Double): Mat {
        var mat = Imgproc.getRotationMatrix2D(Point((image.width() / 2).toDouble(), (image.height() / 2).toDouble()), 0.0, scale)
        Imgproc.warpAffine(image, image, mat, image.size())
        return mat
    }

    fun warp(image: Mat, angle: Double, scale: Double): Mat {
        var mat = Imgproc.getRotationMatrix2D(Point((image.width() / 2).toDouble(), (image.height() / 2).toDouble()), angle, scale)
        Imgproc.warpAffine(image, image, mat, image.size())
        return image
    }

    fun transform(image: Mat): Mat {
        Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2GRAY)

        val paddedImage = Mat()

        val height = Core.getOptimalDFTSize(image.rows())
        val width = Core.getOptimalDFTSize(image.cols())
        Core.copyMakeBorder(image, paddedImage, 0, height - image.rows(),
                0, width - image.cols(), Core.BORDER_DEFAULT)

        paddedImage.convertTo(paddedImage, CvType.CV_32F)

        val magn = FourierTransform.discreteFourrierTransform(paddedImage)
        return magn
    }
}