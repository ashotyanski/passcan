package com.fft

import javafx.application.Application
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage
import org.opencv.core.Core


fun main(args: Array<String>) {
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
    StartProcessing().go(args)
}

class StartProcessing : Application() {
    override fun start(primaryStage: Stage) {
        val loader = FXMLLoader(this::class.java.getResource("/passcan/fxml/FftLayout.fxml"))
        val root = loader.load<AnchorPane>()
        val scene = Scene(root)
        primaryStage.onCloseRequest = EventHandler {
            Platform.exit()
            System.exit(0)
        }
        primaryStage.scene = scene
        primaryStage.show()
    }

    fun go(args: Array<String>) {
        launch(*args)
    }
}
